import os
'''
    This script spawns a new process for each test job to run. 
    This is done because when you run a scrapy job the script it terminated after single use.
'''

if __name__ == "__main__":
    jobs = ["test_pokemon_list", "pokemon_list"] 
    for job in jobs:
        os.system(f"python runner.py {job}")


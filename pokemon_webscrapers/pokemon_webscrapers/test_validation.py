import sys
import os
import json

def validate():
    # validates the data scraped was a success

    #file_to_open = f"../../data/test_pokemon_list.json"
    file_to_open = "test_pokemon_list.json"
    f = open(file_to_open, "r")
    data = json.load(f)
    # with open(file_to_open) as f:
    #     data = json.load(f)
    if data[0]['tests_success']:
        return 0
    else:
        return 1

if __name__ == "__main__":
    validate()